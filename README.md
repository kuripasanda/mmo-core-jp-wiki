Official repository for MMOCore

### MMOCoreを依存関係として使用するには
PhoenixDevリポジトリを登録します
```
<repository>
    <id>phoenix</id>
    <url>https://nexus.phoenixdevt.fr/repository/maven-public/</url>
</repository>
```
そして、MMOCore-APIを依存関係として追加します
```
<dependency>
    <groupId>net.Indyuce</groupId>
    <artifactId>MMOCore-API</artifactId>
    <version>1.12-SNAPSHOT</version>
    <scope>provided</scope>
</dependency>
```
